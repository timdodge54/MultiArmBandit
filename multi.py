import numpy as np
import matplotlib.pyplot as plt
import typing as t
import numpy.typing as npt
import copy
import time

N_ARMS = 21
TIME_HORIZON = 10000
DRIFT_VALUE = 0.001
np.array(
    [
        0,
        -0.5,
        2,
        -0.5,
        -1.2,
        -3,
        -10,
        -0.5,
        -1,
        1,
        0.7,
        -6,
        -7,
        -0.5,
        -6.5,
        -3,
        0,
        2,
        -9,
        -1,
        -4.5,
    ]
)


class MultiArm:
    def __init__(self):
        self.means = np.array(
            [
                0,
                -0.5,
                2,
                -0.5,
                -1.2,
                -3,
                -10,
                -0.5,
                -1,
                1,
                0.7,
                -6,
                -7,
                -0.5,
                -6.5,
                -3,
                0,
                2,
                -9,
                -1,
                -4.5,
            ]
        )

    def get_probabilities(self, i, drift=False):
        if drift:
            self.means -= DRIFT_VALUE
            if i == 3000:
                self.means[0] += 5
                self.means[2] += 2
                self.means[7] += 3
                self.means[18] += 4

        probs = [
            np.random.normal(self.means[0], 5),
            np.random.normal(self.means[1], 12),
            np.random.normal(self.means[2], 3.9),
            np.random.normal(self.means[3], 7),
            np.random.normal(self.means[4], 8),
            np.random.normal(self.means[5], 7),
            np.random.normal(self.means[6], 20),
            np.random.normal(self.means[7], 1),
            np.random.normal(self.means[8], 2),
            np.random.normal(self.means[9], 6),
            np.random.normal(self.means[10], 4),
            np.random.normal(self.means[11], 11),
            np.random.normal(self.means[12], 1),
            np.random.normal(self.means[13], 2),
            np.random.normal(self.means[14], 1),
            np.random.normal(self.means[15], 6),
            np.random.normal(self.means[16], 8),
            np.random.normal(self.means[17], 3.9),
            np.random.normal(self.means[18], 12),
            np.random.normal(self.means[19], 6),
            np.random.normal(self.means[20], 8),
        ]

        return probs

    def epsilon_greedy(self, drift: bool):
        """Perform epsilon-greedy algorithm on the multi-armed bandit problem."""

        epsilons = [0.01, 0.05, 0.1, 0.4]
        epsilon_convergence: t.Dict[float, t.List[t.List[float]]] = {
            epsilon: [] for epsilon in epsilons
        }
        epsilon_percent_right = {
            epsilon: [] for epsilon in epsilons
        }
        total_reward: t.Dict[float, t.List[t.Tuple[int, float]]] = {}

        for epsilon in epsilons:
            values = np.zeros(N_ARMS)
            counts = np.zeros(N_ARMS)
            means = np.zeros(N_ARMS)
            reward_time = []
            reward = 0.0

            def _get_value(epsilon: float, time_step: int) -> t.Tuple[float, int]:
                """Get the value of the action at the current time step."""
                if np.random.random() > epsilon:
                    best_mean = np.max(means)
                    best_index = np.where(means == best_mean)[0][0]
                    if drift == True:
                        return (
                            self.get_probabilities(time_step, drift)[best_index],
                            best_index,
                        )
                    return self.get_probabilities(time_step)[best_index], best_index
                random_index = np.random.randint(0, N_ARMS)
                return self.get_probabilities(time_step)[random_index], random_index
            number_of_correct = 0
            for i in range(TIME_HORIZON):
                value, index = _get_value(epsilon, i)
                values[index] += value
                counts[index] += 1
                means[index] = values[index] / counts[index]
                reward += value
                reward_time.append((i, reward))

                epsilon_convergence[epsilon].append((i, copy.deepcopy(means)))
                if drift == False or i < 3000:
                    if index == 2 or index == 17:
                        number_of_correct += 1
                        epsilon_percent_right[epsilon].append((i,number_of_correct / (i + 1)))
                if drift == True and i > 3000:
                    if index == 0:
                        number_of_correct += 1
                        epsilon_percent_right[epsilon].append((i,number_of_correct / (i + 1)))
            total_reward[epsilon] = reward_time
        self._plot_means(epsilon_convergence, total_reward, epsilon_percent_right, drift)

    def _plot_epsilon_reward(
        self,
        convergences: t.Dict[float, t.List[t.List[float]]],
        drift: bool,
        total_reward: t.Dict[float, t.List[t.Tuple[int, float]]],
        percent_correct: t.Dict[float, t.List[t.Tuple[int, float]]],
    ) -> None:
        """Plot the convergence of the algorithm."""
        for epsilon, convergence in convergences.items():
            time = [epsi[0] for epsi in convergence]
            rewards = [reward[1] for reward in convergence]
            plt.plot(time, rewards, label=f"Epsilon {epsilon}")
        plt.legend()
        if drift == True:
            plt.title("Epsilon Greedy with Drift")
        else:
            plt.title("Epsilon Greedy")
        plt.xlabel("Time step")
        plt.ylabel("Value")
        plt.show()

    def _plot_thompson_means(
        self,
        rewards_time: t.List[t.Tuple[int, float]],
        title: str,
        total_reward: t.List[t.Tuple[int, int]],
        avg_reward: t.List[t.Tuple[int, float]],
        percent_correct: t.List[t.Tuple[int, float]],
    ) -> None:
        for i in range(N_ARMS):
            average_rewards = avg_reward[i]
            x0 = [reward[0] for reward in average_rewards]
            y1 = [reward[1] for reward in average_rewards]
            plt.plot(x0, y1, label=f"Arm {i}")
        plt.ylabel("Average reward")
        plt.xlabel("Time step")
        plt.legend()
        title0 = "Thompson_Sampiling_Avereage_reward_" + title
        plt.title(title0)
        plt.savefig(title0 +".png")
        plt.cla()
        plt.clf()
        for i in range(N_ARMS):
            list_of_means = rewards_time[i]
            time = [reward[0] for reward in list_of_means]
            reward = [reward[1] for reward in list_of_means]
            plt.plot(time, reward, label=f"Arm {i}")
        title1 = "Thompson_Sampling_ " + title
        plt.title(title1)
        plt.legend()
        plt.xlabel("Time step")
        plt.ylabel("Likleyhood of winning")
        plt.savefig(title1 +".png")
        plt.cla()
        plt.clf()
        x = [reward[0] for reward in total_reward]
        y = [reward[1] for reward in total_reward]
        plt.plot(x, y)
        title2 = "Thopson_Sampling_" + title
        plt.title(title2)
        plt.ylabel("Total reward")
        plt.legend()
        plt.savefig(title2+".png")
        plt.cla()
        plt.clf()
        correct = np.array(percent_correct, dtype=[("time", int), ("percent", float)])
        plt.plot(correct["time"], correct["percent"])
        title3 = "Thompson_Sampling_Percent_Correct_" + title
        plt.xlabel("Time step")
        plt.ylabel("Percent Correct")
        plt.savefig(title3+".png")
        plt.cla()
        plt.clf()

    def _plot_means(
        self,
        convergences: t.Dict[float, t.List[t.List[float]]],
        total_reward: t.Dict[float, t.List[t.Tuple[int, float]]],
        percent_correct: t.Dict[float, t.List[t.Tuple[int, float]]],
        drift: bool = False,
    ) -> None:
        """Plot the convergence of the algorithm."""
        for epsilon, e_reward in total_reward.items():
            x = [rew[0] for rew in e_reward]
            y = [rew[1] for rew in e_reward]
            plt.plot(x, y, label=f"Epsilon {epsilon}")
        plt.legend()
        plt.xlabel("Time step")
        plt.ylabel("Total reward")
        title = "Epsilon_Greedy_total_reward_Drift_" + str(drift)
        plt.title(title)
        plt.savefig(title+".png")
        plt.cla()
        plt.clf()
        for epsilon, convergence in convergences.items():
            time = [epsi[0] for epsi in convergence]
            for i in range(N_ARMS):
                x = time
                y = [mean[1][i] for mean in convergence]
                plt.plot(x, y, label=f"Arm {i}")
            plt.legend()
            if drift == True:
                title = f"Epsilon_Greedy_with_Drift_{epsilon}"
                plt.title(title)
            else:
                title = f"Epsilon_{epsilon}"
                plt.title(title)
            plt.xlabel("Time step")
            plt.ylabel("Mean reward")
            plt.savefig(title+".png")
            plt.cla()
            plt.clf()
        for epsilon, percent in percent_correct.items():
            correct = np.array(percent, dtype=[("time", int), ("percent", float)])
            plt.plot(correct["time"], correct["percent"], label=f"Epsilon {epsilon}")
        title2 = f"Percent_correct_Drift_" + str(drift)
        plt.legend()
        plt.title(title2)
        plt.savefig(title2 + ".png")
        plt.cla()
        plt.clf()
            

    def thompson_samp(self, drift: bool, restart: bool):
        restart_init = False
        reward = 0
        reward_time = [[] for _ in range(N_ARMS)]
        count_chosen = [[] for _ in range(N_ARMS)]
        avg_reward = [[] for _ in range(N_ARMS)]
        reward_arm = [[] for _ in range(N_ARMS)]
        total_reward = []
        percent_correct = []

        alpha_beta = np.ones((N_ARMS, 2))
        number_correct = 0
        for i in range(TIME_HORIZON):
            best_beta_val = -np.inf
            best_index = -1
            j = 0
            for alpha, beta in alpha_beta:
                # alpha, beta = alph_bet[0], alph_bet[1]
                beta_val = np.random.beta(alpha, beta)
                ratio_win = alpha / (alpha + beta)
                reward_time[j].append((i, ratio_win))
                if beta_val > best_beta_val:
                    best_beta_val = beta_val
                    best_index = j
                j += 1
            if drift == True:
                if restart and restart_init:
                    alpha_beta = np.ones((N_ARMS, 2))
                    restart_init = True
                value = self.get_probabilities(i, drift)[best_index]
            else:
                value = self.get_probabilities(i)[best_index]
            reward += value
            total_reward.append((i, reward))
            if len(count_chosen[best_index]) == 0:
                count_chosen[best_index].append(1)
                reward_arm[best_index].append(value)
                avg_reward[best_index].append((i, value))
            else:
                count_chosen[best_index].append(count_chosen[best_index][-1] + 1)
                reward_arm[best_index].append(
                    reward_arm[best_index][-1] + value
                )
                avg_reward[best_index].append(
                    (i, reward_arm[best_index][-1] / count_chosen[best_index][-1])
                )
            if value > 0:
                alpha_beta[best_index][0] += 1
            else:
                alpha_beta[best_index][1] += 1
            if drift == False or i < 3000:
                if best_index == 2 or best_index == 17:
                    number_correct += 1
                    percent_correct.append((i, number_correct / (i + 1)))
            if drift == True and i >= 3000:
                if best_index == 0:
                    number_correct += 1
                    percent_correct.append((i, number_correct / (i + 1)))
        title_add = ""
        if drift == True:
            title_add = "with_drift"
            if restart == True:
                title_add += "_and_restart"
        self._plot_thompson_means(reward_time, title_add, total_reward, avg_reward, percent_correct)


if __name__ == "__main__":
    multi0 = MultiArm()
    multi0.epsilon_greedy(False)
    multi0.thompson_samp(False, False)
    multi1 = MultiArm()
    multi1.epsilon_greedy(True)
    print("Thompson Sampling with drift")
    multi2 = MultiArm()
    multi2.thompson_samp(True, False)
    multi3 = MultiArm()
    multi3.thompson_samp(True, True)
