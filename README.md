# MultiArmBandit

### This Multiarm bandit problem has the folowwing distribution per socket

```python3
probs = [
        np.random.normal(0, 5),
        np.random.normal(-0.5,12),
        np.random.normal(2,3.9),
        np.random.normal(-0.5,7),
        np.random.normal(-1.2,8),
        np.random.normal(-3,7),
        np.random.normal(-10,20),
        np.random.normal(-0.5,1),
        np.random.normal(-1,2),
        np.random.normal(1,6),
        np.random.normal(0.7,4),
        np.random.normal(-6,11),
        np.random.normal(-7,1),
        np.random.normal(-0.5,2),
        np.random.normal(-6.5,1),
        np.random.normal(-3,6),
        np.random.normal(0,8),
        np.random.normal(2,3.9),
        np.random.normal(-9,12),
        np.random.normal(-1,6),
        np.random.normal(-4.5,8)              
    ]
```

# Epsilon Greedy

### The results of the greedy epsilon algorithm with 10000 steps showing the mean reward without drift

![Greedy Epsilon .01](figures/Epsilon_0.01.png)
![Greedy Epsilon .05](figures/Epsilon_0.05.png)
![Greedy Epsilon .1](figures/Epsilon_0.1.png)
![Greedy Epsilon .4](figures/Epsilon_0.4.png)

### The results of the greedy epsilon algorithm with 10000 steps showing the mean reward with drift

![Greedy Epsilon Drift .01](figures/Epsilon_Greedy_with_Drift_0.01.png)
![Greedy Epsilon Drift .05](figures/Epsilon_Greedy_with_Drift_0.05.png)
![Greedy Epsilon Drift .1](figures/Epsilon_Greedy_with_Drift_0.1.png)
![Greedy Epsilon Drift .4](figures/Epsilon_Greedy_with_Drift_0.4.png)

### The results of the greedy epsilon algorithm total reward without drift

![Greedy Drif False](figures/Epsilon_Greedy_total_reward_Drift_False.png)

### The results of the greedy epsilon algorithm total reward with drift

![Greedy Drift True](figures/Epsilon_Greedy_total_reward_Drift_True.png)

### The percentage that the greedy epsilon algorithm chose the best socket without drift

![Percent Correct Drift False](figures/Percent_correct_Drift_False.png)

### The percentage that the greedy epsilon algorithm chose the best socket with drift

![Percent Correct Drift True](figures/Percent_correct_Drift_True.png)

# Thompson Sampling

### Average reward of thompson sampling algorithm without drift

![Thompson Sampilng average reward](figures/Thompson_Sampiling_Avereage_reward_.png)

### Average reward of thompson sampling algorithm with drift no restart

![Thompson Sampilng average reward with drift](figures/Thompson_Sampiling_Avereage_reward_with_drift.png)

### Average reward of thompson sampling algorithm with drift and restart

![Thompson Sampilng average reward with drift](figures/Thompson_Sampiling_Avereage_reward_with_drift_and_restart.png)

### Likelhood of winning for each socket without drift

![Thompson Sampling](figures/Thompson_Sampling_%20.png)

### Likelhood of winning for each socket with drift and no restart

![Thompson Sampling with drift](figures/Thompson_Sampling_with_drift.png)

### Likelhood of winning for each socket with drift and restart

![Thompson Sampling with drift](figures/Thompson_Sampling_with_drift_and_restart.png)

### Percent picked best socket without drift

![Thompson Sampling Percent](figures/Thompson_Sampling_Percent_Correct_.png)

### Percent picked best socket with drift and no restart

![Thompson Sampling Percent with drift](figures/Thompson_Sampling_Percent_Correct_with_drift.png)

### Percent picked best socket with drift and restart

![Thompson Sampling Percent with drift](figures/Thompson_Sampling_Percent_Correct_with_drift_and_restart.png)

### Total reward without drift

![Thopson Sampling](figures/Thopson_Sampling_.png)

### Total reward with drift and no restart

![Thopson Sampling](figures/Thopson_Sampling_with_drift.png)

### Total reward with drift and restart

![Thopson Sampling](figures/Thopson_Sampling_with_drift_and_restart.png)

# Problem 1 Answers

* Without drift it seems that .1 allows the algorithm to explore enough to find the best socket but not too much to waste time
* Thompson Sampling seems to converge faster than the greedy epsilon algorithm for every epsilon except .01

# Problem 2 Answers

* The Thompson Sampling algorithm seems to be able to handle the drift better than the greedy epsilon algorithm with greedy epsilon better than every epsilon except .01
* The restart restart seems allows convergence quicker than with no restart
